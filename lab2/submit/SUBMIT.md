
Lab 2: Flow and Congestion Control/Experiment Design
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 2 - Flow and Congestion Control
-------------- | --------------------------------
Name           |Aditi Mayekar 
Net ID         |N19711039
Report due     | Sunday, 22 February 11:59PM


Please answer the following questions:

1) Describe (as specifically as possible) the *goal* of your
experiment. What makes this a good goal? Do not just repeat
all of the indicators of a "good" goal described in the lecture;
explain *how* your selected goal embodies those indicators.
The goal of my experiment was to study delay based congestion control using the throughput with respect to the latency.
It is a good goal because it considers different tcp variants and compares them.

2) What is the TCP congestion control variant you have selected
as the main subject of your study? Describe briefly (1 paragraph)
what characterizes this TCP congestion control, and how it works.
Cite your sources.
HSTCP is selected as the main variant. HSTCP is used during high bandwidth-delay product.
It also recovers packet losses during high bandwidth. 

3) What is the other LFN TCP you have selected to use in your
experiment? What is the non-LFN TCP you have selected to use
in your experiment? Be brief (1 paragraph each), and cite your
sources. Why did you choose these specifically?
The other LFN tcp used is reno. the non lfn tcp used is vegas.
They are chosen so as to provide a broad area of study for comparison.

4) Describe the parameters you have chosen to vary in your
experiment. Why did you choose these? How does this selection help further your stated goal?
The parameters used are tcp variant and link latency. I chose them so as to compare them with the metric that I have used. 
Varying the parameters gives us the idea how it will affect the congestion control.

5) What metrics are you going to measure in your experiment?
Why did you choose these? How does this selection help further your stated goal?
The metric used is throughput which is used with different tcp variants and to compare them.
The throughput for every tcp variant is different. which is what I wish to study in my experiment.

6) For each **experimental unit** in your experiment, describe:

* The specific parameters with which this experimental unit ran
The parameters were link latency and tcp variants,
* The names of all the data files which give results from this experimental unit. Include all of these files in this `submit` folder.
aditi.txt; file.txt; output.txt; file.txt-1
* The specific values of the metrics you have chosen to measure.
The throughput and was measured for every tcp variant.

7) Describe any evidence of *interactions* you can see in the results of your experiment.
The throughput of the TCP vegas was the least while the throughput of tcp highspeed was the highest.

8) Briefly describe the results of your experiment (1-2 paragraphs). You may include images by putting them on an online
image hosting service, then putting the image URL in this file
using the syntax
The result of my experiment was to show that tcp highspeed has the highest throughput and can be used or high bandwidth-delay product.
![](http://link/to/image.png)

9) Find a published paper that studies the same TCP congestion
control variant you have chosen (it may study others as well).
Identify the paper you have chosen with a full citation, and
briefly answer the following questions about it:

 * What research question does this study seek to answer?
 It tells us that the delay based congestion control helps in increasing throughput but increases packet loss.
 * What kind of network environment was this study conducted in?
 They used geni for showing how different tcp variants.
 * How representative is the above network environment of the network setting this TCP variant is designed for?
 The throughput of different tcp variant is studied using the algorthim.
 * Does this study make some comparison to another TCP congestion
 control algorithm? If so, which, and does the author explain why these were selected?
 It shows us how there is packet loss due to congestion control and it introduces the parameter RTT and also discusses about the variation in
Buffer size.
 * What parameters does the author of this study consider? Does the
 author explain why?
 The author has studied different tcp variants, delay and RTT.
 * What metrics does the author of this study consider? Does the author explain why?
 The author has used Throughput as the metric.
 * Critique the experiment(s) in the paper. Does it make any of
 the common mistakes described in the lab lecture? Explain.
 I think the author has mentioned too many details and introduces many concepts on the go which makes it a very complicated goal to study.
 Using many parameters leads to a lot of confusion in the result of the paper.
 
 Citation of the paper studied: http://people.cs.clemson.edu/~jmarty/papers/p356-martin.pdf
